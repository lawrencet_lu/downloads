# .bashrc
#
# ldd command to check lib linked to an executable.
# sshfs name@server:/path/to/folder /path/to/mount/point
# mount -t tmpfs tmpfs /mnt -o size=1024m
alias install-apt='curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash - && \
sudo apt-get install -y curl finger iperf vim ctags cscope \
lsyncd sshfs openssh-server openssh-client sshfs git wipe mosh tmux ruby nodejs '

alias install-climate='git clone https://github.com/adtac/climate.git \
&& cd climate && sudo ./install && cd - '

alias install-mongodb='sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 \
&& echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list \
&& sudo apt-get update \
&& sudo apt-get install -y mongodb-org \
&& sudo service mongod start \
&& ps -ef | grep mongo \
&& climate ports'

alias install-drywall='sudo su -c "gem install sass" && sudo npm install -g grunt-cli bower \
&& git clone https://github.com/arthurkao/angular-drywall.git && cd ./angular-drywall \
&& npm install \
&& cd client && bower install && cd .. '
# run 'node init.js'
# run 'grunt'

alias backupenv='tar -czvf env.gz .vim .vimrc .bin .bashrc .gdbinit .marks .logs .climate'

alias runlsyncd='sudo lsyncd /home/lawrence/work/etc/lsyncd.lua'
# TODO before run lsyncd
# copy lsyncd.lua
# sudo mkdir /var/log/lsyncd
# touch /var/log/lsyncd/lsyncd.{log,status}


# User specific aliases and functions
function c () {
if [ $# = 0 ]; then
	cd && ls --color=auto
else
	cd "$*" && ls --color=auto
fi
}
shopt -s globstar

. ~/.bin/z.sh
. ~/.bin/zz.sh

alias df='df -h'
alias du='du -h'
alias wipe='wipe -r -q -Q 2'
alias rm='rm -i'
alias cp='cp -r -i -a'
alias mv='mv -i'
alias vi='vim'
alias vi='vim'
alias vis="vim -S ~/default_session.vim"
alias arp='arp -n'
alias wcd='find . -name "*.[ch]" | xargs wc -l'
alias lsk='cat ~/.z'
alias tm='tmux'
alias tma='tmux a -t '
alias tmls='tmux ls '
# alias k=$_Z_CMD

alias lsoft='lsot -i tcp -P'
alias ls='ls --color=auto -F'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias lt='ls -rlFth'
alias lta='ls -rlFtah'
alias sl='ls -rlFth'
alias la='ls -aF'
alias lS='ls -lhrS --color=auto'
alias lSa='ls -lhrSa --color=auto'
alias l='ls -F'
alias d='ls -F'
alias acc='ack --cc'
alias af='ack -g'
alias afc='ack --cc -g'
alias ff='fadd && f'
alias fff='fdel && f'
# alias gdb='gdb -tui'
# alias gcc='gcc44'
alias ps='ps --forest -f'
alias psa='ps -ef'
alias psu='ps --forest -f -U'
alias psp='ps --forest -f -C'
alias h='history'
alias rmc='rm -f `find . -name core.* -print`'
alias chcode='chmod a-x *.[ch] Makefile'
alias mp_server='./build/mp_server -c 3 -n 4 -- -p 3 -n 2'
alias mp_client='./build/mp_client -c 1 -n 4 --proc-type=auto -- -n 0 -i 10.1.0.229'

alias cleanpmd='make clean; rm -rf build *.map tags *.out .*~'

# turn cap lock into ESC key
xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
# to undo
# xmodmap -e 'keycode 0x42 = Caps_Lock' -e 'add lock = Caps_Lock'

export PATH=$PATH:$HOME/.bin:/root/.bin:$HOME/.local/bin

export RTE_SDK='/home/lawrence/work/dpdk-2.1.0'
#export RTE_SDK='/home/lawrence/work/dpdk-2.2.0'
# export RTE_SDK='/home/dev/DPDK/'
export RTE_TARGET='x86_64-native-linuxapp-gcc'
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/mpc-0.8.1:/usr/lib/vlc
export VLC_PLUGIN_PATH=/usr/lib/vlc/plugins

export MTCP_DIR='/home/lawrence/work/mtcp-master/mtcp'

ulimit -c unlimited
# ulimit -n 80000
# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
if [ -e /usr/share/terminfo/x/xterm-256color ]; then
export TERM='xterm-256color'
else
export TERM='xterm-color'
fi

##############################################
# the mark and jump commands for directories
export MARKPATH=$HOME/.marks
alias j='jump' # like cd but to dir you marked
alias jl='cd -'
alias jj='marks'
alias m='mark' # mark a dir for use later
alias lsm='marks' # list the marks
alias rmm='unmark' # remove the mark
#################################################

# alias setup='/root/DPDK-1.6.0/tools/runsetup.sh > /dev/null'

function jump { 
# no arg, cd $HOME
if [ -z "$1" ]; then
	cd ~ && pwd && ls -F 
	return
fi

if [ -d "$1" ]; then
	cd $1 && pwd && ls -F 
else
	if [ "$1" == "-" ]; then
		cd -
		return
	fi
#	cd -P $MARKPATH/$1 2>/dev/null && pwd && ls -F 
	cd -P $MARKPATH/$1 2>/dev/null && pwd && ls -F 
fi
}

function mark { 
# mark name path
if [ -n "$2" ]
then
if [ -d "$MARKPATH/$1" ]
then
	rm -f $MARKPATH/$1
fi
	mkdir -p $MARKPATH; ln -s $2 $MARKPATH/$1
	return
fi

# mark name ($pwd)
if [ -n "$1" ]
then
if [ -d "$MARKPATH/$1" ]
then
	rm -f $MARKPATH/$1
fi
	mkdir -p $MARKPATH; ln -s $(pwd) $MARKPATH/$1; export _$1=$(pwd)
	return
fi
}

function mark { 
	mkdir -p $MARKPATH; ln -s $(pwd) $MARKPATH/$1;
}
function unmark { 
    rm -i $MARKPATH/$1 
}
function marks {
	ls -l $MARKPATH | awk '{print ($9, $10, $11)}' | column -t
}

function ackc {
	/usr/local/bin/ack $1 *.[hc]
}
function cv () {
    cd "$@" && pwd && ls
	    }

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
# force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    #PS1='${debian_chroot:+($debian_chroot)}\h@\w:\$ '
#    PS1='${debian_chroot:+($debian_chroot)}\w:\$ '
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
fi
unset color_prompt force_color_prompt

export PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; then echo "$(date "+%Y-%m-%d.%H:%M:%S") $(pwd) $(history 1)" >> ~/.logs/bash-history-$(date "+%Y-%m-%d").log; fi'


source /etc/bash_completion.d/climate_completion
